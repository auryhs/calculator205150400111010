package com.example.calculator;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
//import org.mozilla.javascript.Context;
//import org.mozilla.javascript . Scriptable;


public class MainActivity extends AppCompatActivity{

    EditText edt;
    boolean isNewOp = true;
    String op = "+";
    String oldNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edt = findViewById(R.id.editText);
    }


    public void numberEvent(View view) {
        if(isNewOp)
            edt.setText("");

        isNewOp = false;

        String number = edt.getText().toString();
        switch (view.getId()){
            case R.id.button_6:
                number += "1"; break;
            case R.id.button_7:
                number += "2"; break;
            case R.id.button_8:
                number += "3"; break;
            case R.id.button_9:
                number += "4"; break;
            case R.id.button_10:
                number += "5"; break;
            case R.id.button_11:
                number += "6"; break;
            case R.id.button_12:
                number += "7"; break;
            case R.id.button_13:
                number += "8"; break;
            case R.id.button_14:
                number += "9"; break;
            case R.id.button_5:
                number += "0"; break;
            case R.id.button_18:
                number += "."; break;
        }
        edt.setText(number);
    }

    public void operatorEvent(View view) {
        isNewOp = true;
        oldNumber = edt.getText().toString();
        switch (view.getId()){
            case R.id.button_0: op="+"; break;
            case R.id.button_2: op="-"; break;
            case R.id.button_3: op="*"; break;
            case R.id.button_4: op="/"; break;

        }

    }

    public void equalEvent(View view) {
        String newNumber = edt.getText().toString();
        double result = 0.0;
        switch(op){
            case "+" :
                result = Double.parseDouble(oldNumber) + Double.parseDouble(newNumber);
                break;
            case "-":
                result = Double.parseDouble(oldNumber) - Double.parseDouble(newNumber);
                break;
            case "*" :
                result = Double.parseDouble(oldNumber) * Double.parseDouble(newNumber);
                break;
            case "/" :
                result = Double.parseDouble(oldNumber) / Double.parseDouble(newNumber);
                break;
        }
        edt.setText(result+"");
    }

    public void acEvent(View view) {
        edt.setText("0");
        isNewOp = true;
    }
}